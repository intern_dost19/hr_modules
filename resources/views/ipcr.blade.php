@extends('layouts.main')
@section('after-styles')
    <style>

    </style>
@endsection
@section('scripts')
<script type="text/javascript">
	
</script>
@endsection
@section('content')
<div>

	<h2><i class="fa fa-users"></i> &nbsp Individual Performance Commitment and Review</h2>
	
  <!-- Trigger the modal with a button -->
		<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#loginModal">Add IPCR form</button>
		<br>

	    <table class="table">
	        <br>
	        <tr>
		        <th>Name</th>
		        <th>Division</th>
		        <th>Time Period</th>
		        <th>Operations</th>
	    	</tr>

	    	@foreach ($employee as $employees)
	    	<tr id="<?php echo $employees['id']; ?>">
	    		<td data-target="first_name">{{$employees->first_name}}</td>
	    		<td data-target="last_name">{{$employees->last_name}}</td>
	    		<td data-target="division">{{$employees->division}}</td>
	    		<td data-target="time_period">{{$employees->time_period}}</td>
	    		
	   			<td>
    					<form method='POST' action='/ipcr/{{$employees->id}}'>
							{{csrf_field() }}
							{{method_field('PATCH') }}
							<button type="button" class="btn btn-info"
							data-toggle="modal" data-target="#editModal" id="btnedit" data-role="update"data-id="<?php echo $employees['id'] ;?>">Edit</button>
						
	    				<form method='POST' action='/ipcr/{{$employees->id}}'>
							{{csrf_field() }}
							{{method_field('DELETE') }}
							<button name="save" class="btn btn-danger">Delete</button>
						</form> </form>
	    		</td>
	    	</tr>
	    	@endforeach
	    </table>

<form action="{{ URL::to('/ipcr') }}" method="POST" class="form-horizontal">
	{{ csrf_field() }}
		<!-- Modal LOGIN -->
		<div id="loginModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		      <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Add Employee</h4>

		      </div>
			      <div class="modal-body">

			         	<form action="{{URL::to('/ipcr')}}" method="POST" class="horizontal-forms">
	                        {{ csrf_field() }}
	                        First name
	                        <input type="text" name="first_name" class="form-control" class="col-md-8">
	                        <br>
	                        Last name
	                        <input type="text" name="last_name"  class="form-control" class="col-md-8">
	                        <br>
	                        email
	                        <input type="email" name="email" class="form-control"  class="col-md-8">
	                        <br>
	                        User role
	                        <select name="user_role" class="form-control" class="col-md-8">
			            	<option value="Marksman">Marksman</option>
							<option value="Tank">Tank</option>
							<option value="Assassin">Assassin</option>
							<option value="Fighter">Fighter</option>
			            	<option value="Support">Support</option>
			            	<option value="Mage">Mage</option>
			            	</select>
	                        <br>
                        </form> 
			      </div>
			      <div class="modal-footer">
			    <input type="submit" value="Add" class="btn btn-primary"> 
		        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
		      </div>
		    </div>
		  </div>
		</div>
</form>

			<!-- Modal EDIT-->
		<div id="editModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">
		    -
		      <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Update Employee Record</h4>

		      </div>
			      <div class="modal-body">
			         	<form action="{{URL::to('/ipcr')}}" method="POST" class="horizontal-forms">
	                        {{ csrf_field() }}
	                        First name
	                        <input type="text" name="first_name" class="form-control" class="col-md-8">
	                        <br>
	                        Last name
	                        <input type="text" name="last_name"  class="form-control" class="col-md-8">
	                        <br>
	                        email
	                        <input type="text" name="email" class="form-control"  class="col-md-8">
	                        <br>
	                        User role
	                        <select name="role" class="form-control" class="col-md-8">
			            	<option value="Guard">guard</option>
			            	<option value="Front Desk">Front Desk</option>
			            	<option value="Programmer">Programmer</option>
			            	</select>
	                        <br>                    
                        </form> 
			      </div>
			      <div class="modal-footer">
			    <input  type="submit" onclick="update()" id="submit" value="Update" class="btn btn-primary">

		        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
		      </div>
		    </div>
		  </div>
		</div>
	</div>
@endsection


@section('after-scripts')
<script type="text/javascript">	


</script>
@endsection