<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/ipcr', function () {
    return view('/ipcr');
});


Route::get('/home', 'EmployeeController@create');

Route::post('/home', 'EmployeeController@create');

Route::post('/home', 'EmployeeController@store');

Route::patch('home/{id}', 'EmployeeController@update');

Route::delete('home/{id}', 'EmployeeController@destroy');

//


Route::get('/ipcr', 'EmployeeController@create');

Route::post('/ipcr', 'EmployeeController@create');

Route::post('/ipcr', 'EmployeeController@store');

Route::patch('ipcr/{id}', 'EmployeeController@update');



